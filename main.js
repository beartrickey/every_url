// ====================================================================================================
// Imports
// ====================================================================================================


// Node
const fs = require("fs")
const http = require("http")
const url = require("url")  // Expects node 6.x version of url package

// Third party
const async = require("async")
const request = require("request")
const cheerio = require("cheerio")
const csv = require("csv-to-array")


// ====================================================================================================
// Requests
// ====================================================================================================


// let validDomains = {
// 	"www.atlassian.com": true,
// 	"atlassian.com": true
// }

let validDomains = {
	"www.stride.com": true,
	"stride.com": true
}

let validProtocols = {
	"http:": true,
	"https:": true,
}

let urlDictionary = {}

let expectedResultsList = []

// Store pending URLs to request here. Shift them from the list as we request them
let hopper = []
let pagesParsed = 0
let requestsInFlight = 0

let parsePage = (pageUrlObject)=>{

	console.log("Scanning " + pageUrlObject.href)
	
	// Request page
	let responseBody = ""
	requestsInFlight += 1

	request.get({
		url: pageUrlObject.href,
		followRedirect: false
	})
	.on("response", response=>{

		// Capture status code
		let statusCode = response.statusCode
		urlDictionary[pageUrlObject.href].statusCode = statusCode
		urlDictionary[pageUrlObject.href].redirectLocation = null

		// Add redirect locations to hopper to check later
		if(statusCode >= 300 && statusCode <= 399){
			let redirectLocation = response.headers["location"]
			urlDictionary[pageUrlObject.href].redirectLocation = redirectLocation
			hopperIntake(redirectLocation, pageUrlObject.href)
			requestsInFlight -= 1
			return
		}

		// Skip broken URLs
		if(statusCode != 200){
			requestsInFlight -= 1
			return
		}
		
		response.on("data", data=>{
			responseBody += data
		})
		.on("end", ()=>{

			let html = responseBody
			let $ = cheerio.load(html)
			$("a").each(function(){

				let hrefString = $(this).attr("href")

				// Skip blank strings
				if(hrefString === ""){
					return
				}

				// Skip if null
				if(!hrefString){
					return
				}

				hopperIntake(hrefString, pageUrlObject.href)

			})

			requestsInFlight -= 1

		})

	})
	.on("error", error=>{

		requestsInFlight -= 1
		console.log(error)

	})

}

let hopperIntake = (hrefString, referrer)=>{

	let urlObject = null

	// Make into canonical Node URL Object
	// If the href string is relative (e.g. it starts with a slash /), then
	// the URL Object constructor needs the base domain to fully create it
	let absolutePattern = /^http(s|)/
	if(hrefString.match(absolutePattern)){
		urlObject = url.resolveObject(hrefString, "")
	}else{
		urlObject = url.resolveObject(referrer, hrefString)
	}

	// Skip invalid protocols
	if(!validProtocols[urlObject.protocol]){
		return
	}

	// Skip this URL if already scanned
	if(urlDictionary[urlObject.href]){

		if(referrer){
			urlDictionary[urlObject.href].referrers[referrer] = true
		}

		return
	}

	// Skip if URL is not of valid domain
	if(!validDomains[urlObject.hostname]){
		return
	}

	// Take memo of new URL
	urlDictionary[urlObject.href] = {
		referrers: {}
	}
	if(referrer){
		urlDictionary[urlObject.href].referrers[referrer] = true
	}

	hopper.push(urlObject)

}

let checkHopper = ()=>{

	console.log("")
	console.log("Hopper size: " + hopper.length)
	console.log("pages parsed: " + pagesParsed)
	pagesParsed += 1

	if(hopper.length >= 1){

		let urlObject = hopper.shift()

		try{
			parsePage(urlObject)
		}catch(exception){
			console.log(exception)
		}

		setTimeout(checkHopper, 250)

	}else if(requestsInFlight > 0){

		// If hopper is empty, but there are still requests in progress,
		// check back until they are finished and then exit
		setTimeout(checkHopper, 250)

	}else{
		writeReports()
	}

}

let makeChangeReport = ()=>{

	expectedResultsDictionary = {}
	expectedResultsList.forEach((x)=>{
		expectedResultsDictionary[x.href] = {
			statusCode: x.statusCode,
			redirectLocation: x.redirectLocation
		}
	})
	console.log(expectedResultsList)
	console.log(expectedResultsDictionary)

	// Populate change report
	changeReportText = "href, expectedStatusCode, actualStatusCode, expectedRedirectLocation, actualRedirectLocation, change"
	for(let hrefString in urlDictionary){

		let memoObject = urlDictionary[hrefString]
		let expectedResultObject = expectedResultsDictionary[hrefString]

		console.log(hrefString)
		console.log(memoObject)
		console.log(expectedResultObject)

		// Is this a new URL not in the expected results list?
		if(!expectedResultObject){
			changeReportText += `\n\"${hrefString}\", null, ${memoObject.statusCode}, null, ${memoObject.redirectLocation}, NEW_URL`
		}

		// Was the status different?
		if(expectedResultObject && expectedResultObject.statusCode != memoObject.statusCode){
			changeReportText += `\n\"${hrefString}\", ${expectedResultObject.statusCode}, ${memoObject.statusCode}, null, ${memoObject.redirectionLocation}, STATUS_CHANGE`
		}

		// Was the redirect location different?
		if(
			expectedResultObject && 
			memoObject.redirectLocation && 
			expectedResultObject.redirectLocation != "null" &&
			expectedResultObject.redirectLocation != memoObject.redirectLocation
		){
			changeReportText += `\n\"${hrefString}\", ${expectedResultObject.statusCode}, ${memoObject.statusCode}, ${expectedResultObject.redirectLocation}, ${memoObject.redirectLocation}, REDIRECT_CHANGE`
		}

	}

	let dateString = new Date().toISOString()
	fs.writeFileSync(dateString + "_change_report.csv", changeReportText, {encoding: "utf8"})

}

let loadExpectedResults = (callback)=>{

	// Load expected results csv
	let columns = ["href", "statusCode", "redirectLocation"];
	expectedResultsList = []
	csv(
		{
			file: "expected_responses.csv",
			columns: columns,
			csvOptions: {headers: true}
		}, (err, array)=>{
			if(array){
				expectedResultsList = array

				// Add all csv records into the hopper
				for(let k in expectedResultsList){
					hopperIntake(expectedResultsList[k].href, null)
				}

				callback()
			}
		}
	)

}

let writeReports = ()=>{

	// Need to write synchronously to files when exiting
	let dateString = new Date().toISOString()
	fs.writeFileSync(dateString + "_scanned_urls.json", JSON.stringify(urlDictionary), {encoding: "utf8"})
	fs.writeFileSync(dateString + "_hopper.json", JSON.stringify(hopper), {encoding: "utf8"})

	// Export to csv
	csvString = "url, statusCode, redirectLocation\n"
	for(let k in urlDictionary){
		csvString += "\"" + k + "\", " + urlDictionary[k].statusCode + ", " + urlDictionary[k].redirectLocation + "\n"
	}
	fs.writeFileSync(dateString + "_results.csv", csvString, {encoding: "utf8"})

	// Change report
	makeChangeReport()

	// Finally exit process
	process.exit()

}

// Save what we've collected so far if we ctrl-c to close the program
process.on(
	"SIGINT",
	writeReports
)

// Capture stdin so that node won't exit on its own
process.stdin.resume()

// Seed the crawl with the expected_results.csv file.
// The file should have at least one record with the homepage href
async.series([
	(callback)=>{
		loadExpectedResults(callback)
	},
	(callback)=>{
		checkHopper()
		callback()
	}
])


// ====================================================================================================
// EOF
// ====================================================================================================
